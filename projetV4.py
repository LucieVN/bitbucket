# -*- coding: utf-8 -*-
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import numpy as np
#import pybm3d


#Chargement d'une image
def Lire(path):
    return mpimg.imread(path) # charge l'image et la convertit en matrice couleur directement     


#Enregistrer une image
def Enregistrer(path,img):
    mpimg.imsave(path,img)
    # On remarque ici que les pixels ont des valeurs comprises entre 0 et 1
    #print("Valeurs des pixels dans l'image",img)
    # "matplotlib has rescaled the 8 bits data from each channel to floating point data between 0.0 and 1.0"
    # http://gree2.github.io/python/2015/04/11/python-matplotlib-image-tutorial
 
    
#Afficher une image
def Afficher(img):
    plt.imshow(img)
    plt.gray() # affiche en niveau de gris
    plt.show()
  
    
#Convertit une image en couleur en niveau de gris
# Chaque pixel a une valeur comprise entre 0 et 1
def ColorToBW (img):
    # pour chaque pixel on prend une portion de rouge, vert, bleu, ce qui nous donne une image en niveaux de gris
    # coeffs trouvés sur internet00000000000000000000000000000000
    
    #return 0.21 * img[ : , : , 0] + 0.71 *img[ : , : , 1] + 0.07 * img[ : , : , 2] 
    return 0.299 * img[ : , : , 0] + 0.587 *img[ : , : , 1] + 0.114 * img[ : , : , 2] 
    #return 0.33* img[ : , : , 0] + 0.33 *img[ : , : , 1] + 0.33 * img[ : , : , 2] 
    # "Each inner list represents a pixel. Here, with an RGB image, there are 3 values. 
    # Since it’s a black and white image, R, G, and B are all similar."  
    # https://matplotlib.org/users/image_tutorial.html
    # Ici 0, 1 et 2 représentent les canneaux Rouge, Vert, Bleu.
   
# Pour comprendre à quoi les valeurs des pixels correspondent
def UnderstandPix1 (img):
    dimx=np.shape(img)[0]
    dimy=np.shape(img)[1]
    Img=np.ones((dimx,dimy))
    #img[ : , : , 0] =1
    #img[ : , : , 1] =1 
    #img[ : , : , 2] =1
    return Img


def UnderstandPix0 (img):
    dimx=np.shape(img)[0]
    dimy=np.shape(img)[1]
    Img=np.zeros((dimx,dimy))
    #img[ : , : , 0] =0
    #img[ : , : , 1] =0
    #img[ : , : , 2] =0
    return Img


def UnderstandPix05 (img):
    dimx=np.shape(img)[0]
    dimy=np.shape(img)[1]
    Img=0.5*np.ones((dimx,dimy))
    #img[ : , : , 0] =0
    #img[ : , : , 1] =0
    #img[ : , : , 2] =0
    return Img


def UnderstandPixMoit0Moit1 (img):
    dimx=np.shape(img)[0]
    dimy=np.shape(img)[1]
    Img=np.ones((dimx,dimy)) # toute l'image en pixels =1

    for j in range(int(dimx/2)):
        for i in range(int(dimy/2)):
            Img[j,i] = 0 # une partie de l'image en pixels =0            
    return Img


def UnderstandPixGray(img):
    dimx=np.shape(img)[0]
    dimy=np.shape(img)[1]
    Img=0.5*np.ones((dimx,dimy)) # toute l'image en pixels =1

    for j in range(int(dimx/2)):
        for i in range(int(dimy/2)):
            Img[j,i] = 0 # une partie de l'image en pixels =0
            
            
    for j in range(int(dimx/5)):
        for i in range(int(dimy/8)):
            Img[j,i] = 0.9 # une partie de l'image en pixels =0        
    return Img
      
         
#Flouter une image
def Flouter (img,sigma):
    dimx=np.shape(Img)[0]
    dimy=np.shape(Img)[1]
    
    filtre_flou = Filtre(sigma, dimx, dimy)

    #Passage en Fourier
    Ffiltre_flou=np.fft.fft2(filtre_flou) #Fourier en 2-D du bruit
    Fimg=np.fft.fft2(img)  #Fourier en 2-D de l'image
    # REVOIR LA POSITION DE LA TRANSPOSEE POUR UNE IMAGE RECTANGULAIRE
    FimgFloue=(Ffiltre_flou.transpose())*Fimg #Convolution terme à terme
    
    #Passage en Discret
    imgFloue=np.fft.ifft2(FimgFloue).real
    return imgFloue


def Filtre (sigma, dimx, dimy):
    #création de la grille
    x=np.linspace(-dimx/2.,dimx/2.-1.,dimx)
    y=np.linspace(-dimy/2.,dimy/2.-1.,dimy)
    X,Y=np.meshgrid(x,y) 
    
    #création du filtre flou
    filtre_flou=np.exp(-(X**2+Y**2)/(2*sigma**2))
    filtre_flou=filtre_flou/sum(sum(filtre_flou[:,:])) #Normalisation
    filtre_flou=np.fft.fftshift(filtre_flou) #Centrage
    
    return filtre_flou


# Déflouter une image  
# Semble ne pas fonctionner -> critère d'arrêt à revoir ? 
def DeFlouter(ImgFloue,sigma, nitermax=100):
    dimx=np.shape(ImgFloue)[0]
    dimy=np.shape(ImgFloue)[1]
    Img=np.zeros((dimx,dimy))
    
    H=Filtre(sigma, dimx, dimy)
    #Passage en Fourier
    FH=(np.fft.fft2(H)).transpose() # à faire : régler la transposée dans la création du filtre
    FImgFloue=np.fft.fft2(ImgFloue)
    FImg=np.copy(FImgFloue)
    #gradient accélérée
    FImgold=np.copy(FImgFloue)
    yk=np.copy(FImgFloue)
    

    # méthode de la puissance pour estimer la plus grande valeur propre
    # car L est la plus grande vp de HTH
    nitermaxVP=50
    ValP=ImgFloue
    ValPold=ImgFloue*0
    cpt1=0
    tol1=1e-8
    while (cpt1 < nitermaxVP)and (np.abs(ValP-ValPold).all()>tol1):
        ValPold=np.copy(ValP)
        ValP=np.fft.ifft2(np.conj(FH)*FH*ValP).real
        ValP=ValP/np.linalg.norm(ValP)
        cpt1+=1
    #constante de Lipschitz
    L=np.linalg.norm(ValP)
    pas=2/L #pas optimal
    if (cpt1 < nitermaxVP):
        print("sortie boucle while car plus grande valeur propre trouvee ")
    else :
        print("sortie boucle while car nb iteration max atteint pour recherche valeur propre.")
    
    
    # calcul du gradient
    # faire gradient accéléré à la place du gradient
    # définir criètre d'arrêt : norme 2 du grad de psi <tol
    cpt2=0
    tol2=1e-4
    gradPsi=np.conj(FH)*(FH*FImg-FImgFloue)/sigma**2 # pour pouvoir rentrer dans la boucle
    StockGradPsi=[] # pour stoquer les valeurs de GradPsi et afficher la fonction coût
# =============================================================================
#     while ((cpt2 < nitermax) and (np.linalg.norm(gradPsi, ord=2) > tol2)):
#         gradPsi=np.conj(FH)*(FH*FImg-FImgFloue)/sigma**2 # produit terme à terme
#         StockGradPsi.append(np.linalg.norm(gradPsi, ord=2)) # fonction coût dans le vecteur StockGradPsi
#         ## descente de gradient
#         FImg=FImg-pas*gradPsi
#         cpt2+=1
# =============================================================================
        
    #gradient accéléré 
    while ((cpt2 < nitermax) and (np.linalg.norm(gradPsi, ord=2) > tol2)):
        FImgold=np.copy(FImg)
        gradPsi=np.conj(FH)*(FH*FImg-FImgFloue)/sigma**2 # produit terme à terme
        StockGradPsi.append(np.linalg.norm(gradPsi, ord=2)) # fonction coût dans le vecteur StockGradPsi
        FImg=yk-pas*gradPsi
        yk=FImg+((cpt2-1.)/(cpt2+2.))*(FImg-FImgold)
        cpt2+=1
        
    if (cpt2 < nitermax):
        print("sortie boucle while car gradient nul")
    else :
        print("sortie boucle while car nb iteration max atteint pour descente de gradient")
    
    # Passage en discret
    Img=np.fft.ifft2(FImg).real
    # Pour pouvoir observer la différence entre l'image floue et l'image censée être défloutée:
    # print(Img-ImgFloue)
    
    # affichage de la fonction coÛt
    plt.figure()
    plt.title("Evolution de la fonction coût")
    plt.xlabel("Nombre d'itérations")
    plt.ylabel("Valeur de la fonction coût (gradPsi)")
    plt.plot(StockGradPsi) 
    plt.show()
    return Img


 # affichage de SNR (image de départ et celle qu'on veut évaluer)
 # Plus SNR est élevé et mieux l'image "tend" vers l'image que l'on cherche à retrouver
def SNR(x_ref,x):
    return -20*np.log10(np.linalg.norm(x_ref-x)/np.linalg.norm(x));
    #return (np.linalg.norm(x_ref-x, ord=2));
    
 
#Bruiter (http://www.f-legrand.fr/scidoc/docimg/numerique/filtre/bruit/bruit.html)
def Bruiter (Img,sigma):
    dimx=np.shape(Img)[0] # dimension de x
    dimy=np.shape(Img)[1]
    ImgBruite=np.zeros((dimx,dimy))
    for j in range(dimx):
        for i in range(dimy):
            ImgBruite[j,i] =Img[j,i]+np.random.normal(0,sigma) #ajout d'un bruit aléatoire (centré en 0 car le bruit est en moyenne nul)  
    return ImgBruite
    

# Debruiter avec BM3D 
# Source de l'algorithme :
# https://github.com/gfacciol/bm3d/tree/54c41cb0f6b8c26c068e2149315a8c71236715d5
# https://github.com/ericmjonas/pybm3d   
# Ne fonctionne pas (même sur un test simple issu de la doc en ligne sur GitHub)
# =============================================================================
# def BM3D (Img, sigma): 
#     dimx=np.shape(Img)[0] # dimension de x
#     dimy=np.shape(Img)[1]
#     ImgDebruite=np.zeros((dimx,dimy))
#     # Utilisation de BM3D pour débruiter l'image
#     ImgDebruite=pybm3d.bm3d.bm3d(Img, sigma)
#     return ImgDebruite
# =============================================================================






#Principal    
    
pathLire=r"\Users\lucie\Documents\Cours\4 INSA\Projet\Code\Lenna.png" 
#pathLire=r"C:\Users\paupa\Documents\INSA\DefloutageAveugle\groot.png"
#pathLire=r"/home/pauline/Bureau/Defloutage/groot.png"
pathEnregistrer=r"\Users\lucie\Documents\Cours\4 INSA\Projet\Code\Lennabis.png" 
#pathEnregistrer=r"/home/pauline/Bureau/Defloutage/groot_enregistre.png" 
      
sigmaFloue=5.
sigmaBruit=0.1 
sigmaDeflou=5.

Img=Lire(pathLire)
ImgBW=ColorToBW(Img)
print("Image originale que l'on cherche à retrouver :")
Afficher(ImgBW)

#ImgFloue=Flouter(ImgBW,sigmaFloue)
#print("Image floue :")
#Afficher(ImgFloue)
#print("SNR de l'image floutée", SNR(ImgBW,ImgFloue))

ImgBruit=Bruiter(ImgBW,sigmaBruit)
print("Image bruitée :")
Afficher(ImgBruit)
#
#ImgDeb=BM3D(Img,sigmaBruit)
#print("Image debruitée :")
#Afficher(ImgDeb)

#ImgBruiteFloue=Bruiter(ImgFloue,sigmaBruit)
#print("Image floue et bruitée : ")
##Afficher(ImgBruiteFloue)
#
#Enregistrer(pathEnregistrer,ImgBruiteFloue)
#print("Image défloutée :")
#ImgDeflou=DeFlouter(ImgFloue, sigmaDeflou)
#Afficher(ImgDeflou) 
#print("SNR de l'image défloutée", SNR(ImgBW,ImgDeflou))
Enregistrer(pathEnregistrer,ImgBruit)
 

## Test des valeurs des pixels pour essayer de comprendre quelle valeur fait du blanc et quelle valeur fait du noir 
## Les valeurs sont comprises entre 0 et 1
#print("Image avec tous les pixels à 1 :")
#ImgPix1=UnderstandPix1(ImgBW)
#Afficher(ImgPix1)
#
#print("Image avec tous les pixels à 0 : ")
#ImgPix0=UnderstandPix0(ImgBW)
#Afficher(ImgPix0)
#
#print("Image avec tous les pixels à 0.5 : ")
#ImgPix05=UnderstandPix05(ImgBW)
#Afficher(ImgPix05)
#
#print("Image avec des pixels à 1 et des pixels à 0")
#ImgPixMoit=UnderstandPixMoit0Moit1(ImgBW)
#Afficher(ImgPixMoit)
#
#print("Image avec des pixels à 0.5, des pixels à 0 et des pixels à 0.9")
#ImgPixGray=UnderstandPixGray(ImgBW)
#Afficher(ImgPixGray)

# De ces tests on en conclut que :
# si il n'y a qu'une seule valeur de pixels sur l'image, l'image est noire
# si il y a 2 valeurs différentes, on obtient les extrêmes : noir et blanc
# si il y a 3 valeurs de pixels différentes, on obtient du noir, du blanc et du gris intermédiaire
# Etc pour plus de valeurs



